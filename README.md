# Guessing Game GUI

## Introduction

> A mini game where the computer have to guess your number!

## Detailed Function

The computer tries to guess your number by asking you whether the number is bigger or smaller.
In the end it will show you the number of tries.

## Screenshots

| [![Hai](https://i.ibb.co/J3RD5nR/image.jpg)](https://gitlab.com/haidepzai/guessing-game-gui)

| [![Hai](https://i.ibb.co/ZdDB4yw/1.png)](https://gitlab.com/haidepzai/guessing-game-gui)

| [![Hai](https://i.ibb.co/T23m3j1/2.png)](https://gitlab.com/haidepzai/guessing-game-gui)
