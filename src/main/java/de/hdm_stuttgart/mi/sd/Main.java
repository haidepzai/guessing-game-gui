package de.hdm_stuttgart.mi.sd;

import java.awt.*;

public class Main {

    public static void main(String[] args) {

        /*
        EventQueue.invokeLater(new Runnable() {
            public void run() {
         */

        EventQueue.invokeLater(() -> {
            try {
                GuessingGameGUI window = new GuessingGameGUI();
                window.frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

}
