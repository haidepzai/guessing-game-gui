package de.hdm_stuttgart.mi.sd;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.Font;

import javax.swing.SwingConstants;

public class GuessingGameGUI {

    public JFrame frame;
    private static int compCount = 1;
    private JLabel textField = new JLabel("<html>Denke dir eine Zahl zwischen 1 und 1000 aus! <br> \r\nBist du bereit?\r\n</html>");
    private JLabel numField = new JLabel("");
    private static boolean firstTurn = true; //Beim Klicken von Button "Ja" = false; Erneutes klicken => Text: Anzahl Versuche

    int max = 1000;
    int min = 1;
    int guessingNumber = (max + min) / 2;


    /**
     * Constructor
     * Create the application.
     */
    public GuessingGameGUI() {
        initialize();
    }

    private void Guess(){
        textField.setText("<html>Ist das die gesuchte Zahl?<br></html>");
        numField.setText(String.valueOf(guessingNumber));

    }

    public static int nextGuess(int max, int min) {
        return (max + min) / 2;

    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame("Guessing Game by Hai");
        frame.setBounds(100, 100, 561, 464);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null); //Kein Layout -> Button beliebig setzbar
        frame.getContentPane().setBackground(new Color(255, 236, 154));

        //textField:
        textField.setHorizontalAlignment(SwingConstants.CENTER);
        textField.setFont(new Font("Yu Gothic", Font.BOLD | Font.ITALIC, 24));
        textField.setBounds(82, 25, 355, 161);
        frame.getContentPane().add(textField);


        numField.setFont(new Font("Yu Gothic", Font.BOLD | Font.ITALIC, 32));
        numField.setHorizontalAlignment(SwingConstants.CENTER);
        numField.setBounds(143, 208, 235, 76);
        frame.getContentPane().add(numField);

        JButton btnYes = new JButton("Ja");
        btnYes.setFont(new Font("Yu Gothic", Font.BOLD | Font.ITALIC, 26));
        btnYes.addActionListener(arg0 -> {
            if (firstTurn) {
                Guess();
                firstTurn = false;
            } else {
                textField.setText("<html>Easy!!<br>Habe nur soviele Versuche gebraucht:</html>");
                numField.setText(String.valueOf(compCount));
            }
        });
        btnYes.setBounds(31, 316, 145, 76);
        btnYes.setBackground(new Color(0x0C9C00));
        btnYes.setBorder(BorderFactory.createLineBorder(Color.ORANGE, 4));
        frame.getContentPane().add(btnYes);

        JButton btnBigger = new JButton("Gr\u00F6\u00DFer");
        btnBigger.addActionListener(e -> {
            min = guessingNumber;
            guessingNumber = nextGuess(max, min);
            compCount++;
            Guess();
        });
        btnBigger.setFont(new Font("Yu Gothic", Font.BOLD | Font.ITALIC, 23));
        btnBigger.setForeground(Color.WHITE);
        btnBigger.setBounds(186, 317, 152, 76);
        btnBigger.setBackground(new Color(0x3133BA));
        btnBigger.setBorder(BorderFactory.createLineBorder(Color.ORANGE, 4));
        frame.getContentPane().add(btnBigger);

        JButton btnLesser = new JButton("Kleiner");

        /*
        btnLesser.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
         */

        btnLesser.addActionListener(e -> {
            max = guessingNumber;
            guessingNumber = nextGuess(max, min);
            compCount++;
            Guess();
        });
        btnLesser.setFont(new Font("Yu Gothic", Font.BOLD | Font.ITALIC, 23));
        btnLesser.setForeground(Color.WHITE);
        btnLesser.setBounds(348, 317, 148, 76);
        btnLesser.setBackground(new Color(0xAB0200));
        btnLesser.setBorder(BorderFactory.createLineBorder(Color.ORANGE, 4));
        frame.getContentPane().add(btnLesser);

    }
}
